import {Button, Form, Row, Col, Container} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import userContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Login(){

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	const {user, setUser} = useContext(userContext);

	useEffect(()=>{
		if(email !== "" && password !== ""){
			setIsActive(true);
			}
		else{
			setIsActive(false);
		}	
	}, [email, password,]);

	function loginUser(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URI}/users/login`, {

			method: "POST",
			headers: {
				'Content-Type': 'application/json'},
				body: JSON.stringify({
					email: email,
					password: password
				})
			
		}).then(response => response.json())
		.then(data => {
			console.log(data);
			if(data.accessToken !== "empty"){
				localStorage.setItem('token', data.accessToken);
				retrieveUserDetails(data.accessToken);
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to our website!"
				})
			}else{
				Swal.fire({
					title: "Authentication failed!",
					icon: "error",
					text: "Check your login details and try again!"
				})
				setPassword('');
			}
		})

		const retrieveUserDetails = (token) => {
			fetch(`${process.env.REACT_APP_API_URI}/users/profile`,
				{headers: {
					Authorization: `Bearer ${token}`
				}})
				.then(response => response.json())
				.then(data => {
					console.log("@USER", data);
					setUser({id: data._id, isAdmin: data.isAdmin})
			})
		}
	}
	return(
		(user.id !== null || undefined) ?
			<Navigate to = "/"/>
		:
		<Container>
			<Row>
				<Col className = "col-md-4 col-8 offset-md-4 offset-2 mt-2 justify-content-center">
					<Form onSubmit = {loginUser} className = "p-3">
					      <Form.Group className="mb-3" controlId="formBasicEmail">
					        <Form.Label>Email address</Form.Label>
					        <Form.Control
					         type="email" 
					         placeholder="Enter email" 
					         value = {email}
					         onChange = {event => setEmail(event.target.value
					         	)}
					         required/>
					      </Form.Group>

				      <Form.Group className="mb-3" controlId="password1">
				        <Form.Label>Enter Your Password</Form.Label>
				        <Form.Control 
				        type="password" 
				        placeholder="Password"
				        onChange = {event => setPassword(event.target.value)}
				        value = {password}
				        required/>
				      </Form.Group>

				      <Button className = "col-md-4 col-8 offset-md-4 offset-2 mt-2" variant="outline-success" type="submit" disabled = {!isActive}>
				        Login
				      </Button>
				    </Form>
		    	</Col>
			</Row>	
		</Container>
		)
}