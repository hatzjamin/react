const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(
		{
			firstName : {
				type : String,
				required : [true, "name is required."]
			},
			lastName : {
				type : String,
				required : [true, "name is required."]
			},
			email : {
				type : String,
				required : [true, "Email is required."],
				unique: true
			},
			password : {
				type : String,
				required : [true, "Password is required."]
			},
			mobileNo : {
			type : String, 
			required : [true, "Mobile No is required"]
			},
			cart: [{
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Cart',
			  }],
			isAdmin : {
				type : Boolean,
				default : false
			}
		},
		{
			timestamps: true
		}
	)

module.exports = mongoose.model("User", userSchema);
