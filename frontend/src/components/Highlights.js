// import of the classes needed for the CRC rule as well as the classes needed for the bootstrap component
import {Row, Col, Card} from 'react-bootstrap'

export default function Highlights(){

	return(
		<Row className = "my-3">
	{/*This is the first card*/}
			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3 text-center">
				      <Card.Body>
				        <Card.Title>
				        <h2>Acoustic Guitars</h2>
				        </Card.Title>
				        <Card.Text>
				          Acoustic guitars form several notable subcategories within the acoustic guitar group: classical and flamenco guitars; steel-string guitars, which include the flat-topped, or "folk", guitar; twelve-string guitars; and the arched-top guitar. The acoustic guitar group also includes unamplified guitars designed to play in different registers, such as the acoustic bass guitar, which has a similar tuning to that of the electric bass guitar.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

		{/*This is the 2nd card*/}
			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3 text-center">
				      <Card.Body>
				        <Card.Title>
				        <h2>Classical Guitars</h2>
				        </Card.Title>
				        <Card.Text>
				          Classical guitars, also known as "Spanish" guitars, are typically strung with nylon strings, plucked with the fingers, played in a seated position and are used to play a diversity of musical styles including classical music. The classical guitar's wide, flat neck allows the musician to play scales, arpeggios, and certain chord forms more easily and with less adjacent string interference than on other styles of guitar.
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>

		{/*This is the 3rd card*/}
			<Col xs={12} md = {4}>
				<Card className = "cardHighlight p-3 text-center">
				      <Card.Body>
				        <Card.Title>
				        <h2>Electric Guitars</h2>
				        </Card.Title>
				        <Card.Text>
				         Electric guitars can have solid, semi-hollow, or hollow bodies; solid bodies produce little sound without amplification. In contrast to a standard acoustic guitar, electric guitars instead rely on electromagnetic pickups, and sometimes piezoelectric pickups, that convert the vibration of the steel strings into signals, which are fed to an amplifier through a patch cable or radio transmitter. 
				        </Card.Text>
				      </Card.Body>
				    </Card>
			</Col>
		</Row>
		)
}