const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers");
const auth = require("../auth.js");

// Adding product
router.post("/add", auth.verify, productControllers.addProduct);

// All active products
router.get("/allActiveProducts", productControllers.getAllActive);

// retieve all products
router.get("/allProducts", auth.verify, productControllers.getAllProducts);

// retrieve single product
router.get("/:productId", productControllers.getSingleProduct);

// update a product
router.put("/update/:productId", auth.verify, productControllers.updateProduct);

// archive/unarchive product
router.patch("/archive/:productId", auth.verify, productControllers.archiveProduct);

// unarchive a product
router.patch("/:productId/unarchive", auth.verify, productControllers.unArchiveProduct);


module.exports = router;