//Admin Dashboard contains all the modification: Update, Archive, Unarchive, 

import {Button, Col, Row, Container, Table} from "react-bootstrap";
import {useEffect, useState, useContext} from "react";
import UserContext from "../UserContext";
import {useNavigate, Navigate, Link, useParams} from "react-router-dom";
import Swal from 'sweetalert2';

export default function AdminDashboard() {
  
  const {user} = useContext(UserContext);
  const [products, setProducts] = useState([]);
  const navigate = useNavigate();
  const [isUpdated, setIsUpdated] = useState(false); //Modified
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URI}/products/allProducts`, {
      headers: {"Content-Type" : "application/json", //Modified
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setProducts(data);
      });
      setIsLoading(false);
  }, [isUpdated]);

  const Unarchive = (id) => {
    fetch(`${process.env.REACT_APP_API_URI}/products/${id}/unarchive`, {
        method: "PATCH",
        headers: {"Content-Type" : "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`},
          body: JSON.stringify({
            isActive: true
          })
      })
        .then((response) => response.json())
        .then((data) => {
          console.log("Hello", data);

          if (data) {
            Swal.fire({
              title: "Product Successfully Unarchive!",
              icon: "success",
              text: "Product activated.",
            });

            products.map(product => {
              if(product._id === id){
                console.log("Friday")
                
                setIsUpdated(u => !u);
              }
            })

          }

          else {
            Swal.fire({
              title: "Product Archived Failed",
              icon: "error",
              text: "Failure to deactivate the product, please try again.",
            });
          }
        });
  }


  function archive(id){
      console.log(id);
      console.log(products)
      fetch(`${process.env.REACT_APP_API_URI}/products/archive/${id}`, {
        method: "PATCH",
        headers: {"Content-Type" : "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`},
          body: JSON.stringify({
            isActive: false
          })
      })
        .then((response) => response.json())
        .then((data) => {
          console.log("Hi", data);

          if (data) {
            Swal.fire({
              title: "Product Successfully Archived!",
              icon: "success",
              text: "Product deactivated.",
            });

            products.map(product => {
              if(product._id === id){
                console.log("Friday")
                
                setIsUpdated(u => !u);
              }
            })

          }

          else {
            Swal.fire({
              title: "Product Archived Failed",
              icon: "error",
              text: "Failure to deactivate the product, please try again.",
            });
          }
        });
    }

  return ( isLoading ? <h4>Loading</h4>
    :
    <div className="p-5 ">
      <Container className="bg-light rounded shadow-lg">
        <Row className="justify-content-center text-center mx-3 my-3">
        
        <Table striped bordered hover className="m-4 shadow-sm">
          <thead className="bg-black text-light">
            <tr>

              <th>Title</th>
              <th>Description</th>
              <th>Price</th>
              <th>Stocks</th>
              <th>Active</th>
              <th>Modify</th>
            </tr>
          </thead>
          {products.length !== 0 &&

          <tbody className="text-left">
             {products.map((product) => {
              return (
                <tr key={product._id}>
                  <td>{product.title}</td>
                  <td>{product.description}</td>
                  <td>{product.price}</td>
                  <td>{product.stock}</td>
                  <td>{String(product.isActive)}</td>
                  <td>
                    {product.isActive ? (
                      <div className="mx-3 my-3 shadow-sm">
                        <Button
                          onClick = {() => archive(product._id)}
                          variant="outline-warning">
                          Archive
                        </Button>
                      </div>

                    ) : (
                      <div className="mx-3 my-3 shadow-sm">
                        <Button
                          onClick = {() => Unarchive(product._id)}
                          variant="outline-warning">
                          Unarchive
                        </Button>
                      </div>
                      
                    )}
                    <div className="mx-3 my-3 yellow1 rounded shadow-sm">
                      <Button
                        as={Link}
                        to={`/adminUpdate/${product._id}`}
                        variant="outline-dark">
                        Update
                      </Button>
                    </div>
                    
                  </td>
                </tr>
              );
            })}
          </tbody>
        }
        </Table>
      </Row>
      </Container>
      
    </div>
  );
}