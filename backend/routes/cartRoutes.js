// dependencies
const express = require("express");
const router = express.Router();

// modules
const cartController = require('../controllers/cartController');
const checkoutController = require('../controllers/checkoutController');
const auth = require('../auth');

// USER: retrieve cart
router.get('/', auth.verify, cartController.getUserCart);

// add to cart
router.post("/addtocart/:productId", auth.verify, cartController.addToCart);

// remove from cart
// router.delete("/removefromcart/:cartItemId", auth.verify, cartController.removeFromCart);

// Checkout cart item
router.post("/checkout/:cartItemId", auth.verify, checkoutController.checkout);

module.exports = router;