// importing modules using deconstruct
import {Container, Nav, Navbar} from 'react-bootstrap';
import {Fragment, useContext} from 'react'
import {NavLink} from "react-router-dom";
import userContext from '../UserContext';
import bocchitar from '../images/bocchitar-logo.png'

export default function AppNavbar(){

  const { user } = useContext(userContext);

	return(
		 <Navbar bg="secondary" expand="lg" className="vw-100">
      <Container fluid>
{/*        <Navbar.Brand as = {NavLink} to = "/" className = "text-light">LogoExample</Navbar.Brand>*/}
        <Navbar.Brand as = {NavLink} to = "/">
                          <img src={bocchitar} className="logoBranding" fluid="true" alt="Logo" />
        </Navbar.Brand >
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto text-light" >
            {
              (user.id !== null) 
              ? (user.isAdmin === true)
              ? <>
                <Nav.Link as = {NavLink} to = "/dashboard" className="text-light">Dashboard</Nav.Link>
                <Nav.Link as = {NavLink} to = "/allProducts" className="text-light">All Products</Nav.Link>
                <Nav.Link as = {NavLink} to = "/add" className="text-light">Create Product</Nav.Link>
                <Nav.Link as = {NavLink} to = "/logout" className="text-light">Logout</Nav.Link>
                </>
              : <>
                <Nav.Link as = {NavLink} to = "/" className="text-light">Home</Nav.Link>
                <Nav.Link as = {NavLink} to = "/products" className="text-light">Products</Nav.Link>
                <Nav.Link as = {NavLink} to = "/cart" className="text-light">Cart</Nav.Link>
                <Nav.Link as = {NavLink} to = "/logout" className="text-light">Logout</Nav.Link>
                </>
              : <>
                <Nav.Link as = {NavLink} to = "/" className="text-light">Home</Nav.Link>
                <Nav.Link as = {NavLink} to = "/products" className="text-light">Products</Nav.Link>
                <Nav.Link as = {NavLink} to = "/register" className="text-light">Register</Nav.Link>
                <Nav.Link as = {NavLink} to = "/login" className="text-light">Login</Nav.Link>
                </>
            }
            
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
		)
}