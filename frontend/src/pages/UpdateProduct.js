//Admin Dashboard Page: Update Product information

import {Button, Col, Row, Container, Card} from "react-bootstrap";
import {useState, useEffect, useContext} from "react";
import {useParams, useNavigate} from "react-router-dom";
import UserContext from "../UserContext";
import Form from "react-bootstrap/Form";
import Swal from "sweetalert2";
// import { useUserContext } from '../hooks/useUserContext';


export default function UpdateProduct() {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [stock, setStock] = useState("");
  const [isActive, setIsActive] = useState(true);
  const [product, setProduct] = useState([]);
  const {productId} = useParams();
  
  const {user} = useContext(UserContext);
  const navigate = useNavigate();


  useEffect(() => {
    if (
      title !== "" && description !== "" && price !== "" && stock !== ""){
      setIsActive(false);
    } else {
      setIsActive(true);
    }
  }, [title, description, price, stock]);


  async function updateProduct(event) {
    event.preventDefault();
    console.log(productId)

    const response = await fetch(
        `${process.env.REACT_APP_API_URI}/products/update/${productId}`, {
          method: "PUT",
          headers: {"Content-Type": "application/json",
            Authorization: `Bearer ${localStorage.getItem("token")}`
          },
        body: JSON.stringify({
        title: title,
        description: description,
        price: price,
        stock: stock
        })
      });

      const json = await response.json();
      console.log(json)

      if (!response.ok) {
        Swal.fire({
            title: "Update Failed",
            icon: "error",
            text: "Please check if the information you input are correct.",
          });
        
        console.log(json.error);
        
      }

      if (response.ok) {
        /*productsDispatch({ type: 'SET_ACTIVE_PRODUCTS', payload: json });*/
        Swal.fire({
            title: "Product Successfully Updated",
            icon: "success",
            text: "Product information has been updated",
          });

        navigate("/dashboard")
      }
  }

  return (
    <Container className = "my-4">
      <Row className="mt-5">
        <Col className = "col-md-5 col-8 offset-md-4 offset-2">
          
          <Card className="p-4 rounded shadow-lg">
            <Form onSubmit={updateProduct} className="p-3">

                <Form.Group controlId = "productName">
                  <Form.Label className = "fw-bold">Title</Form.Label>
                  <Form.Control
                    className = "mb-3"
                    type = "text"
                    placeholder = "Update Product Name"
                    value = {title}
                    onChange = {event => setTitle(event.target.value)}
                    required/>
                </Form.Group>

                <Form.Group controlId = "productPrice">
                  <Form.Label className = "fw-bold">Price</Form.Label>
                  <Form.Control
                    className = "mb-3"
                    type = "number"
                    placeholder = "Update Price"
                    value = {price}
                    onChange = {event => setPrice(event.target.value)}
                    required/>
                </Form.Group>

                <Form.Group controlId = "productStock">
                  <Form.Label className = "fw-bold">Count in Stock</Form.Label>
                  <Form.Control
                    className = "mb-3"
                    type = "number"
                    placeholder = "Update Stocks"
                    value = {stock}
                    onChange = {event => setStock(event.target.value)}
                    required/>
                </Form.Group>

                <Form.Group controlId = "productDescription" className = "mb-3">
                  <label
                    for="exampleFormControlTextarea1"
                    class="form-label"
                    className = "fw-bold">Description</label>
                  <textarea
                    class="form-control"
                    id="exampleFormControlTextarea1"
                    rows="3"
                    value = {description}
                    placeholder = "Update Description"
                    onChange = {event => setDescription(event.target.value)}
                    required></textarea>
                </Form.Group>


                <Button
                  className = "yellow1 text-black shadow-sm offset-4"
                  variant = "outline-success"
                  type = "submit">
                  Update Product
                </Button>
            </Form>
          </Card>
          
        </Col>
      </Row>
    </Container>
  );
}
