// modules
import {Button, Form, Row, Col, Container, Table} from 'react-bootstrap';
import styled from 'styled-components';
import Navbar from '../components/AppNavbar';
import { Link } from 'react-router-dom';
import React, { useContext, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

const AddProduct = () => {
    const [title, setTitle] = useState("");
    const [price, setPrice] = useState('');
    const [description, setDescription] = useState("");
    const [stock, setStock] = useState('');
    const [isActive, setIsActive] = useState(false);

    const navigate = useNavigate();

    const {user} = useContext(UserContext);

    useEffect(() => {
        title !== "" && description !== "" && price !== ""  && stock !== ""
        ? setIsActive(true)
        : setIsActive(false)

    }, [title, description, price, stock, isActive]);

    const addProduct = event => {
        event.preventDefault();

        console.log(user)
        if(user.isAdmin){
            fetch(`${process.env.REACT_APP_API_URI}/products/add`, {
                method: "POST",
                headers: {
                    'Content-Type' : 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    title,
                    description,
                    price,
                    stock
                })
                
            }).then(response => response.json())
            .then(data =>{
                console.log("@CREATE", data);
                if(data){
                        Swal.fire({
                            title: "Product Created",
                            icon: "success",
                            text: "Thank you for your patience"
                        })

                        navigate('/dashboard');
                    }
                })
            } else {
            Swal.fire({
                title: "Access Denied",
                icon: "success",
                text: "You are not authorized to do this type of action."
            })
        }
    }


    return(
        <>  
            <Container className = "center">
                
                    <title>Add Product</title>
                    <Form onSubmit={addProduct}>
                        <input className = "input1"
                            type="text"
                            placeholder="Name"
                            value={title}
                            onChange={event => setTitle(event.target.value)}
                            required/>
                            
                        <input className = "input2"
                            type="text"
                            placeholder="Description"
                            value={description}
                            onChange={event => setDescription(event.target.value)}
                            required/>
                            <br/>
                        <input className = "input3"
                            type="number"
                            placeholder="Price"
                            value={price}
                            onChange={event => setPrice(event.target.value)}
                            required/>  
                            <br/> 
                        <input className = "input4"
                            placeholder="Stock"
                            type="number"
                            value={stock}
                            onChange={event => setStock(event.target.value)}
                            required/>
                        <br/> 

                        <Button variant = "outline-success" type="submit" disabled={!isActive}>Add Product</Button>
                       
                    </Form>

            </Container>
        </>

  )
}

export default AddProduct

